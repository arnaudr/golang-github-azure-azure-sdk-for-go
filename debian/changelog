golang-github-azure-azure-sdk-for-go (36.2.0-1) UNRELEASED; urgency=medium

  [ Alexandre Viau ]
  * Team upload.
  * Point Vcs-* urls to salsa.debian.org.

  [ Martín Ferrari ]
  * debian/gbp.conf: Update repo layout.

  [ Dr. Tobias Quathamer ]
  * New upstream version 20.1.0
  * Update Standards-Version to 4.2.1, no changes needed
  * Update dependencies for new upstream version

  [ Arnaud Rebillout ]
  * New upstream version 36.2.0
  * Remove reference to Gododir in d/rules
  * Update copyright
  * Update build dependencies
  * Comment out build dependencies that are not packaged
  * Build and install "storage" only, exclude the rest
  * Update maintainer email address
  * Bump standards version
  * Update debian compat to 12
  * Add Rules-Requires-Root to control file

 -- Arnaud Rebillout <arnaud.rebillout@collabora.com>  Tue, 19 Nov 2019 15:14:00 +0700

golang-github-azure-azure-sdk-for-go (10.3.0~beta-1) unstable; urgency=medium

  * Team upload.

  [ Paul Tagliamonte ]
  * Remove Built-Using from arch:all -dev package

  [ Shengjing Zhu ]
  * New upstream release. (Closes: #859360)
  * Update compat and debhelper to 10
  * Update Standards-Version to 4.1.0
    + Change priority to optional
    + User secure url in copyright format
  * d/watch: Add filename mangle to fix upstream beta name
  * d/control:
    + Update pkg-go team name
    + Update Build-Depends and Binary Depends
    + Add Testsuite
  * d/rule: exclude Gododir dir and storage dir when test
  * Add patch 0001-Rename-satori-uuid-to-satori-go.uuid.patch

 -- Shengjing Zhu <i@zhsj.me>  Tue, 05 Sep 2017 09:32:54 +0800

golang-github-azure-azure-sdk-for-go (2.1.1~beta-3) unstable; urgency=medium

  [ Paul Tagliamonte ]
  * Use a secure transport for the Vcs-Git and Vcs-Browser URL

  [ Martín Ferrari ]
  * Add myself to Uploaders.
  * debian/control: Update Standards-Version (no changes).
  * debian/control: Remove golang-go dependency from -dev package.
  * debian/control: Replace golang-go with golang-any in Build-Depends.
  * debian/control: Drop golang-check.v1-dev as an alternative dependency.

 -- Martín Ferrari <tincho@debian.org>  Mon, 31 Oct 2016 22:03:56 +0000

golang-github-azure-azure-sdk-for-go (2.1.1~beta-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Daniel Stender <stender@debian.org>  Mon, 25 Apr 2016 12:22:36 +0200

golang-github-azure-azure-sdk-for-go (2.1.1~beta-1) experimental; urgency=medium

  * Team upload.
  [ Tianon Gravi ]
  * Adjust Build-Depends relation ordering to appease the buildds
  [ Daniel Stender ]
  * New upstream release (Closes: #821832).
    + added golang-golang-x-crypto-dev to deps in deb/control.
    + bumped version of go-autorest deb in deb/control.
    + dropped export for DH_GOLANG_INSTALL_ALL in deb/rules.
  * deb/rules: added failsafe for dh_auto_test (Closes: #821933).

 -- Daniel Stender <stender@debian.org>  Wed, 20 Apr 2016 20:35:26 +0200

golang-github-azure-azure-sdk-for-go (1.2~git20150611.0.97d9593-2) unstable; urgency=medium

  * Note that core/* is BSD-3-clause and copyright of "The Go Authors"; thanks
    Thorsten!

 -- Tianon Gravi <tianon@debian.org>  Mon, 14 Sep 2015 19:19:55 -0700

golang-github-azure-azure-sdk-for-go (1.2~git20150611.0.97d9593-1) unstable; urgency=medium

  * Initial release

 -- Tianon Gravi <tianon@debian.org>  Fri, 11 Sep 2015 17:48:41 -0700
